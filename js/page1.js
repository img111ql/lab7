jQuery.noConflict();

jQuery(document).ready(function ($) {
    // convenience function - make RED border

    function redBorder() {
        $(this).css('border', '4px red solid');
    }

    // $('#bton01').css('border', '3px solid red');

    // // change border of all li elements
    // $('li').css('border', '3px solid red');
    // $('li').each(function(){
    //     $(this).css('border', '5px solid green');
    // })

    // $('li').each(redBorder);
    // $('p').each(redBorder);

    //-----------------------------------------------------------------//
    // click on button 1 to increase size of the font of the first
    // paragraph by 10%
    $('#btn01').on('click', function () {
        var $par1 = $('p').eq(0);
        //$par1.each(redBorder);
        var textSize = parseInt($par1.css("fontSize"));
        textSize = parseInt(textSize);
        var newTextSize = Math.floor(textSize * 1.1);
        $par1.css('fontSize', newTextSize + "px");
    });

    // click on button 10 will first slideUp image 1 in 2.5 secs followed
    // by slideUp of image 2 in 2.5 sec
    $('#btn10').on('click', function () {
        var $firstImage = $('img').eq(0);
        var $secondImage = $('img').eq(1);
        $firstImage.slideUp(2500, function () {
            $secondImage.slideUp(2500);
        });
    });

    // click on Button11 will slide up and down image number 2 in 3 seconds
    $('#btn11').on('click', function () {
        var $image = $('img').eq(1);
        $image.slideUp(1500, function () {
            $image.slideDown(1500);
        });
        //$image.slideToggle(1500);
    });

    // click on Button5 should replace the answer column in the "mytable"
    // with random images
    $('#btn05').on('click', function () {
        $('table#mytable tr').each(function () {
            var randImageName = pictures[Math.floor(Math.random() * pictures.length)];
            var $myImage = $('<img width ="50" height="50" />');
            $myImage.attr("src", "images/" + randImageName);
            console.log(randImageName);
            $(this).find('td').eq(1).each(function () {
                $(this).html($myImage);
            });
        });
    });

    // mouseover event should change first image to half size and mouseout
    // event should restore the size
    var imageWidth = $('img').eq(0).attr("width");
    imageWidth = parseInt(imageWidth);
    $('img:first').on('mouseover mouseleave', function () {
        var $myImage = $('img').eq(0);
        var myWidth = $myImage.attr("width");
        myWidth = parseInt(imageWidth);
        if (myWidth === imageWidth) {
            $myImage.attr("width", imageWidth / 2);
        } else {
            $myImage.attr("width", imageWidth);
        }
    });


    // Click on Button3 and hide first 4 paragraphs in a smooth motiong (3 sec)
    $('#btn03').on('click', function () {
        $('p:lt(4)').hide(3500);
        $('p:lt(4)').show(3500);
    });


    // Click on Button9 to fade out or fade in image 2 in 2500 msec
    $('#btn09').on('click', function () {
        $('img:eq(1)').fadeToggle(2500);
    });


    // animate the third image from its size to 400
    $('img:eq(2)').animate({
        "width": "400px",
        "height": "400px",
        opacity: 0.4
    }, 3000);


    // move the first image to the right in a slow motion
    $firstImage = $('img:first');
    $firstImage.css('position', 'relative');
    // var maxWidth = screen.width;
    var imageWidth = $firstImage.attr("width");
    var maxWidth = $(window).width();
    console.log(maxWidth);

    function moveRight(duration) {
        $firstImage.animate({
            left: maxWidth - (imageWidth * 1.6)
        }, duration);
    }

    function moveLeft(duration) {
        $firstImage.animate({
            left: 0
        }, duration);
    }

    //

    var stop = true;
    var start = false;
    var index = 0;

    $('#btn12').on('click', function () {
        while (index < 100) {
            moveRight(3000);
            moveLeft(3000);
            index++;
            console.log(index);
        }
    });

});